
<!-- README.md is generated from README.Rmd. Please edit that file -->

# elisedupuis

<!-- badges: start -->
<!-- badges: end -->

The goal of elisedupuis is to create a simple R package for the level 3
training.

## Installation

You can install the development version of elisedupuis like so:

``` r
remotes::install_local(path = "path/to/elisedupuis_0.0.0.9000.tar.gz", 
                        build_vignettes = TRUE)
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(elisedupuis)
dire_bonjour(prenom = "Elise")
#> Bonjour, Elise
```

## Pays dataset

The “pays” dataset is available in the following way:

``` r
data("pays", package = "elisedupuis")
#> Warning in data("pays", package = "elisedupuis"): data set 'pays' not found
```

## Kitten picture

A kitten picture is available with the package

``` r
system.file("chaton.png", package = "elisedupuis")
#> [1] ""
```
