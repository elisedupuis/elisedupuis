## code to prepare `pays` dataset goes here
pays <- readr::read_csv("data-raw/pays.csv", col_names = FALSE)


usethis::use_data(pays, overwrite = TRUE)
checkhelper::use_data_doc("pays")
rstudioapi::navigateToFile("R/doc_pays.R")
